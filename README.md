## Node OAuth server

### Purpose

This project uses the simple-oauth2 node module for authenticating a user into a service that uses the oauth2 authorization flow.

This project consists of multiple branches:

* **master** contains a template for using simple-oauth2.

* **oauth-automatic** contains specific example of accessing the Automatic api

* **oauth-spotify** contains specific example of accessing the Spotify api

* **oauth-fitbit** contains specific example of accessing FitBit api

### Apps/APIs to add

Future apps to add include:

* Twitter
* Pinterest
* Google+

Some platforms have oauth2 schemes wrapped in their own SDKs.  Those implementations more than likely will not work with the simple-oauth2 module.  Google and Facebook provide their own SDKs and html buttons for logging in.

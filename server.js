//server.js

var express = require('express');
var session = require('express-session');
var port = process.env.PORT || 4000;
var app = express();

// Enable sessions
app.use(session({
  secret: 'coppertosteeltoahingethatisfaltered',
  resave: false,
  saveUninitialized: true
}));

// app client id and client secret config
var CLIENT_ID = process.env.CLIENT_ID || "client id";
var CLIENT_SECRET = process.env.CLIENT_SECRET || "client secret";

var oauth2 = require('simple-oauth2')({
  clientID: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  site: 'https://accounts.someplace.com',
  tokenPath: '/oauth/access_token',
  authorizationPath: '/oauth/authorize'
});

// Authorization uri definition
var authorization_uri = oauth2.authCode.authorizeURL({
  redirct_uri: 'http://localhost:4000/redirect',
  scope: 'scope:public',
  state: 'blahblah'
});

// Initial page redirecting to apps's oAuth page
app.get('/auth', function (req, res) {
  res.redirect(authorization_uri);
});

// Callback service parsing the authorization token and asking for the access token
app.get('/redirect', function (req, res) {
  var code = req.query.code;
  var tokenData = null;

  oauth2.authCode.getToken({
    code: code,
    redirect_uri: 'http://localhost:4000/redirect'
  },
  function saveToken(error, result) {
    if(error) {
      console.log('access token error' + error.message);
    }

    tokenData = oauth2.accessToken.create(result);

    //save token data in session or a db store
    req.session.token = tokenData;

    res.redirect('/dashboard');
  });
});

app.get('/dashboard', function (req, res) {

  if(req.session.token) {

    // Display token to authenticated user
    console.log('Access Token:' + req.session.token.token.access_token);
    res.send('You are logged in.<br>Access Token: ' +  req.session.token.token.access_token + '<br>Refresh Token: ' + req.session.token.token.refresh_token);
  } else {

    // No token, so redirect to login
    res.redirect('/');
  }
});

// start route
app.get('/', function (req, res) {
  res.send('<a href="/auth">Log in with App</a>');
});

// start server
app.listen(port);
console.log('Express server started on port ' + port);
